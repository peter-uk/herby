package com.pc.herby.data.api;


import com.pc.herby.recipePOJO.ResultsID;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RecipeApiService {

    @GET("product/")
    Single<ResultsID> getRecipeIDs(@Query("ingredients") String ingredients);

}
