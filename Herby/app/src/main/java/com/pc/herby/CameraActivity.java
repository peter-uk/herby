package com.pc.herby;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.camerakit.CameraKit;
import com.camerakit.CameraKitView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.document.FirebaseVisionDocumentText;
import com.google.firebase.ml.vision.document.FirebaseVisionDocumentTextRecognizer;
import com.google.firebase.ml.vision.objects.FirebaseVisionObject;
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetector;
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions;
import com.pc.herby.UI.Injection;
import com.pc.herby.data.local.ProductDb;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener {
    private CameraKitView cameraKitView;
    private ImageView btn_capture, btn_flashlight;
    private String flashMode = "";
    private AlertDialog dialog;
    private Context mContext;
    private int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Objects.requireNonNull(getSupportActionBar()).hide();
        mContext = this;

        cameraKitView = findViewById(R.id.camera);
        btn_capture = findViewById(R.id.btn_capture);
        btn_flashlight = findViewById(R.id.btn_flashlight);
        cameraKitView.requestPermissions(this);

        if (flashMode.isEmpty() || flashMode.equals("off")) {
            flashMode = "off";
            flash("off");
        }
        flash(flashMode);
        btn_flashlight.setOnClickListener(this);
        btn_capture.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        dialog = builder.create();

    }

    public void flash(String mode) {
        switch (mode) {
            case "off":
                cameraKitView.setFlash(CameraKit.FLASH_OFF);
                btn_flashlight.setImageDrawable(getDrawable(R.drawable.ic_flash_off_white_24dp));
                break;
            case "on":
                cameraKitView.setFlash(CameraKit.FLASH_ON);
                btn_flashlight.setImageDrawable(getDrawable(R.drawable.ic_flash_on_white_24dp));
                break;
            case "auto":
                cameraKitView.setFlash(CameraKit.FLASH_AUTO);
                btn_flashlight.setImageDrawable(getDrawable(R.drawable.ic_flash_auto_white_24dp));
                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        cameraKitView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraKitView.onResume();
    }

    @Override
    protected void onPause() {
        cameraKitView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        cameraKitView.onStop();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(this, "You must allow permission to use the camera", Toast.LENGTH_SHORT).show();
            finish();
        }
        cameraKitView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onClick(View view) {
        if (view == btn_capture) {
            cameraKitView.captureImage(new CameraKitView.ImageCallback() {
                @Override
                public void onImage(CameraKitView cameraKitView, final byte[] capturedImage) {
                    // capturedImage contains the image from the CameraKitView.
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(capturedImage, 0, capturedImage.length, opts);
                    FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
                    //barcodeProcessor(image);
                    value = getIntent().getIntExtra("id",0);
                    Log.i("onImage: ", String.valueOf(value));
                        switch (value) {
                            case R.id.action_barcode: //barcode
                                barcodeProcessor(image);
                                break;
                            case R.id.action_cam: //item
                                itemProcessor(image);
                                // code block
                                break;
                            case R.id.action_receipt: //receipt
                                textProcessor(image);
                                break;
                        }
                        value = 0;
                        //The key argument here must match that used in the other activity
                    dialog.show();
                    cameraKitView.onStop();
                }
            });
        } else if (view == btn_flashlight) {
            switch (flashMode) {
                case "off":
                    flashMode = "on";
                    flash("on");
                    break;
                case "on":
                    flashMode = "auto";
                    flash("auto");
                    break;
                case "auto":
                    flashMode = "off";
                    flash("off");
                    break;
            }


        }
    }

    public void itemProcessor(FirebaseVisionImage image) {
        FirebaseVisionObjectDetectorOptions options =
                new FirebaseVisionObjectDetectorOptions.Builder()
                        .setDetectorMode(FirebaseVisionObjectDetectorOptions.SINGLE_IMAGE_MODE)
                        .enableClassification()  // Optional
                        .build();

        FirebaseVisionObjectDetector objectDetector =
                FirebaseVision.getInstance().getOnDeviceObjectDetector(options);

        objectDetector.processImage(image)
                .addOnSuccessListener(
                        new OnSuccessListener<List<FirebaseVisionObject>>() {
                            @Override
                            public void onSuccess(List<FirebaseVisionObject> detectedObjects) {
                                Integer id = null;
                                Rect bounds;
                                int category = 0;
                                Float confidence;
                                for (FirebaseVisionObject obj : detectedObjects) {
                                    id = obj.getTrackingId();
                                    bounds = obj.getBoundingBox();
                                    category = obj.getClassificationCategory();
                                    confidence = obj.getClassificationConfidence();
                                }
                                dialog.dismiss();
                                Intent intent = new Intent(CameraActivity.this, TextActivity.class);
                                intent.putExtra("result", category);
                                startActivity(intent);
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Task failed with an exception
                                // ...
                            }
                        });

    }

    public void barcodeProcessor(FirebaseVisionImage image) {
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
                .getVisionBarcodeDetector();
        Task<List<FirebaseVisionBarcode>> res = detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                        String rawValue = null;
                        for (FirebaseVisionBarcode barcode : barcodes) {
                            rawValue = barcode.getRawValue();
                        }
                        Toast.makeText(CameraActivity.this,rawValue, Toast.LENGTH_LONG).show();
                        barcode(rawValue);
                        dialog.dismiss();

                        //code for displaying gtin in text activity
                        //Intent intent = new Intent(CameraActivity.this, TextActivity.class);
                        //intent.putExtra("result", rawValue);
                        //startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                    }
                });


    }

    public void textProcessor(FirebaseVisionImage image) {
        FirebaseVisionDocumentTextRecognizer detector = FirebaseVision.getInstance()
                .getCloudDocumentTextRecognizer();

        detector.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<FirebaseVisionDocumentText>() {
                    @Override
                    public void onSuccess(FirebaseVisionDocumentText result) {
                        dialog.dismiss();
                        Intent intent = new Intent(CameraActivity.this, TextActivity.class);
                        intent.putExtra("result", result.getText());
                        startActivity(intent);
                        //overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle("Processing Failed");
                        builder.setMessage(e.getLocalizedMessage());
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                cameraKitView.onStart();
                                cameraKitView.onResume();
                            }
                        }).show();
                    }
                });
    }

    //removes un-needed info from the receipt
    private String receiptCleanUp(String x){
        String textBetween = between(x,"www.tesco.com/store-locator", "TOTAL TO PAY").replaceAll("\\s+[a-zA-Z\"*\"]\\s+", "\n"); // removes single chars and *
        String replaceCurrency = textBetween.replaceAll("\\p{Sc}","£"); // switches currency to £
        return before(replaceCurrency,"TOTAL TO PAY").replaceAll("^(?:[\\t ]*(?:\\r?\\n|\\r))+","");
    }

    static String between(String value, String a, String b) {
        // Return a substring between the two strings.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        int posB = value.lastIndexOf(b);
        if (posB == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= posB) {
            return "";
        }
        return value.substring(adjustedPosA, posB);
    }

    static String before(String value, String a) {
        // Return substring containing all characters before a string.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        return value.substring(0, posA);
    }

    private static final String TAG = MainActivity.class.getSimpleName();
    private MainViewModel viewModel;
    private MainViewModelFactory factory;

    //searches API and adds the item to the rooms DB
    private void barcode(String gtin){
        //String gtin = String.valueOf(code);
        //Toast.makeText(CameraActivity.this,gtin, Toast.LENGTH_LONG).show();
        factory = Injection.provideViewModelFactory(this, gtin);
        viewModel = new ViewModelProvider(this, factory).get(MainViewModel.class);
        viewModel.getProductList().observe(this,productList -> {
            Log.i(TAG, "observeProductList : " + productList.size());
           if (!productList.isEmpty()) {
                //int x = productList.size();
                ProductDb productDb = productList.get(0);
               Toast.makeText(CameraActivity.this,
                       "Brand: "+productDb.getBrand() + "\n" +
                               "Description : " +productDb.getDescription()
                       , Toast.LENGTH_LONG).show();


               Log.i(TAG, "product info : gtin " + productDb.getGtin()
                        + " quantity:" + productDb.getQtyContents().getQuantity()
                        + "quantityTotal: " + productDb.getQtyContents().getTotalQuantity());
            }
        });
    }
}