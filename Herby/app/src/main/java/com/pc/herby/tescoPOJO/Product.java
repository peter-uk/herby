
package com.pc.herby.tescoPOJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Product {

    @Expose
    @SerializedName("gtin")
    private String gtin;

    @SerializedName("tpnb")
    @Expose
    private String tpnb;

    @SerializedName("tpnc")
    @Expose
    private String tpnc;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("qtyContents")
    @Expose
    private QtyContents qtyContents;

    @SerializedName("catId")
    @Expose
    private String catId;

    @SerializedName("productCharacteristics")
    @Expose
    private ProductCharacteristics productCharacteristics;

    @SerializedName("pkgDimensions")
    @Expose
    private List<PkgDimension> pkgDimensions = null;

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public String getTpnb() {
        return tpnb;
    }

    public void setTpnb(String tpnb) {
        this.tpnb = tpnb;
    }

    public String getTpnc() {
        return tpnc;
    }

    public void setTpnc(String tpnc) {
        this.tpnc = tpnc;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public QtyContents getQtyContents() {
        return qtyContents;
    }

    public void setQtyContents(QtyContents qtyContents) {
        this.qtyContents = qtyContents;
    }

    public ProductCharacteristics getProductCharacteristics() {
        return productCharacteristics;
    }

    public void setProductCharacteristics(ProductCharacteristics productCharacteristics) {
        this.productCharacteristics = productCharacteristics;
    }

    public List<PkgDimension> getPkgDimensions() {
        return pkgDimensions;
    }

    public void setPkgDimensions(List<PkgDimension> pkgDimensions) {
        this.pkgDimensions = pkgDimensions;
    }

    public Product() {

    }
}
