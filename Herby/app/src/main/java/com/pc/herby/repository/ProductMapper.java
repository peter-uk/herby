package com.pc.herby.repository;

import com.pc.herby.tescoPOJO.Product;
import com.pc.herby.data.local.ProductDb;

public final class ProductMapper {

  private ProductMapper(){

  }

    public static ProductDb toProductDb(Product product){
        return new ProductDb(Long.valueOf(product.getGtin()) , product.getTpnb(),
                product.getTpnc(), product.getDescription() , product.getBrand(),
                product.getQtyContents());
    }
}
