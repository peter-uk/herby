package com.pc.herby;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class HerbyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initStetho();

    }
    private void initStetho() {
        Stetho.initializeWithDefaults(this);
    }
}
