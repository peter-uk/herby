package com.pc.herby.UI.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.pc.herby.R;
import com.pc.herby.UI.Recipes.ExampleItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.FoodViewHolder>{
    private Context mContext;
    private ArrayList<recipesData> myFoodList;

    public static class FoodViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView nTitle,nDescription;
        CardView nCardView;

        public FoodViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.ivImage);
            nTitle = itemView.findViewById(R.id.tvTitle);
            nDescription = itemView.findViewById(R.id.tvDescr);
            nCardView = itemView.findViewById(R.id.MyCardView);

        }
    }

    public HomeAdapter(ArrayList<recipesData> recipesData){
        myFoodList = recipesData;
    }

    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View nView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_row_item,viewGroup,false);
        return new FoodViewHolder(nView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder foodViewHolder, int position) {
        foodViewHolder.nTitle.setText(myFoodList.get(position).getItemName());
        foodViewHolder.nDescription.setText(myFoodList.get(position).getItemDescription());

        recipesData food = myFoodList.get(position);
        String x = food.getItemImage();
        Picasso.get().load(x).fit().centerCrop().into(foodViewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return myFoodList.size();
    }
}


