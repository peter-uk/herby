package com.pc.herby.UI.Checkout;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CheckoutViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CheckoutViewModel() {
    }

    public LiveData<String> getText() {
        return mText;
    }
}