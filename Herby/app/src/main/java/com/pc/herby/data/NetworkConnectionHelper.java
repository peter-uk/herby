package com.pc.herby.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

public class NetworkConnectionHelper {

    private Context context;

    public NetworkConnectionHelper(Context context) {
        this.context = context;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) return false;

        Network networkCapabilities = connectivityManager.getActiveNetwork();
        NetworkCapabilities activeNetwork = connectivityManager.getNetworkCapabilities(networkCapabilities);

        if (activeNetwork == null) return false;

        if (activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
            return true;
        else if (activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
            return true;
        else if (activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
            return true;
        else
            return false;
    }
}
