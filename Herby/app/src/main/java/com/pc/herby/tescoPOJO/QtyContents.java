
package com.pc.herby.tescoPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QtyContents {

    @SerializedName("quantity")
    @Expose
    private Double quantity;

    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;

    @SerializedName("quantityUom")
    @Expose
    private String quantityUom;

    @SerializedName("unitQty")
    @Expose
    private String unitQty;

    @SerializedName("netContents")
    @Expose
    private String netContents;

    @SerializedName("avgMeasure")
    @Expose
    private String avgMeasure;

    @SerializedName("numberOfUnits")
    @Expose
    private String numberOfUnits;

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getQuantityUom() {
        return quantityUom;
    }

    public void setQuantityUom(String quantityUom) {
        this.quantityUom = quantityUom;
    }

    public String getUnitQty() {
        return unitQty;
    }

    public void setUnitQty(String unitQty) {
        this.unitQty = unitQty;
    }

    public String getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(String numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getNetContents() {
        return netContents;
    }

    public void setNetContents(String netContents) {
        this.netContents = netContents;
    }

    public String getAvgMeasure() {
        return avgMeasure;
    }

    public void setAvgMeasure(String avgMeasure) {
        this.avgMeasure = avgMeasure;
    }
}
