
package com.pc.herby.tescoPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductCharacteristics {

    @SerializedName("isFood")
    @Expose
    private Boolean isFood;
    @SerializedName("isDrink")
    @Expose
    private Boolean isDrink;
    @SerializedName("isHazardous")
    @Expose
    private Boolean isHazardous;
    @SerializedName("storageType")
    @Expose
    private String storageType;
    @SerializedName("isAnalgesic")
    @Expose
    private Boolean isAnalgesic;
    @SerializedName("containsLoperamide")
    @Expose
    private Boolean containsLoperamide;

    public Boolean getIsFood() {
        return isFood;
    }

    public void setIsFood(Boolean isFood) {
        this.isFood = isFood;
    }

    public Boolean getIsDrink() {
        return isDrink;
    }

    public void setIsDrink(Boolean isDrink) {
        this.isDrink = isDrink;
    }

    public Boolean getIsHazardous() {
        return isHazardous;
    }

    public void setIsHazardous(Boolean isHazardous) {
        this.isHazardous = isHazardous;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public Boolean getIsAnalgesic() {
        return isAnalgesic;
    }

    public void setIsAnalgesic(Boolean isAnalgesic) {
        this.isAnalgesic = isAnalgesic;
    }

    public Boolean getContainsLoperamide() {
        return containsLoperamide;
    }

    public void setContainsLoperamide(Boolean containsLoperamide) {
        this.containsLoperamide = containsLoperamide;
    }

}
