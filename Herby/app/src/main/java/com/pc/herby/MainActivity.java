package com.pc.herby;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.internal.NavigationMenu;
import com.pc.herby.UI.Injection;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = MainActivity.class.getSimpleName();
    private MainViewModel viewModel;
    private MainViewModelFactory factory;
    private long code = 5000157062772L;
    private String gtin = String.valueOf(code);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        factory = Injection.provideViewModelFactory(this, gtin);
        //testProductList(); //TEST DB ADD

        //FAB CODE
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        FabSpeedDial fabSpeedDial = findViewById(R.id.fabSpeedDial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {

            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                // TODO: Do something with menu items, or return false if you don't want to show them
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                // TODO: Start some activity
                int itemId = menuItem.getItemId();
                //Toast.makeText(MainActivity.this,menuItem.getTitle(), Toast.LENGTH_LONG).show();
                if (itemId == R.id.ic_view) { // manual mode
                    startActivity(new Intent(getApplicationContext(), TextActivity.class));
                    return true;
                }
                if (itemId == R.id.action_manual) { // manual mode
                    startActivity(new Intent(getApplicationContext(), TextActivity.class));
                    return true;
                } else {
                    //sends FAB menu id to camera activity to change what happens to image
                    Intent i = new Intent(MainActivity.this, CameraActivity.class);
                    i.putExtra("id", itemId);
                    startActivity(i);
                    //startActivity(new Intent(getApplicationContext(), CameraActivity.class));
                    return true;
                }
            }
        });
    }
}
