
package com.pc.herby.recipePOJO;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultsID implements Serializable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("imageType")
    @Expose
    private String imageType;
    @SerializedName("usedIngredientCount")
    @Expose
    private int usedIngredientCount;
    @SerializedName("missedIngredientCount")
    @Expose
    private int missedIngredientCount;
    @SerializedName("missedIngredients")
    @Expose
    private List<MissedIngredient> missedIngredients = null;
    @SerializedName("usedIngredients")
    @Expose
    private List<UsedIngredient> usedIngredients = null;
    @SerializedName("unusedIngredients")
    @Expose
    private List<Object> unusedIngredients = null;
    @SerializedName("likes")
    @Expose
    private int likes;
    private final static long serialVersionUID = 9183777090456836191L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResultsID() {
    }

    /**
     * 
     * @param image
     * @param usedIngredients
     * @param missedIngredients
     * @param missedIngredientCount
     * @param unusedIngredients
     * @param id
     * @param title
     * @param imageType
     * @param usedIngredientCount
     * @param likes
     */
    public ResultsID(int id, String title, String image, String imageType, int usedIngredientCount, int missedIngredientCount, List<MissedIngredient> missedIngredients, List<UsedIngredient> usedIngredients, List<Object> unusedIngredients, int likes) {
        super();
        this.id = id;
        this.title = title;
        this.image = image;
        this.imageType = imageType;
        this.usedIngredientCount = usedIngredientCount;
        this.missedIngredientCount = missedIngredientCount;
        this.missedIngredients = missedIngredients;
        this.usedIngredients = usedIngredients;
        this.unusedIngredients = unusedIngredients;
        this.likes = likes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public int getUsedIngredientCount() {
        return usedIngredientCount;
    }

    public void setUsedIngredientCount(int usedIngredientCount) {
        this.usedIngredientCount = usedIngredientCount;
    }

    public int getMissedIngredientCount() {
        return missedIngredientCount;
    }

    public void setMissedIngredientCount(int missedIngredientCount) {
        this.missedIngredientCount = missedIngredientCount;
    }

    public List<MissedIngredient> getMissedIngredients() {
        return missedIngredients;
    }

    public void setMissedIngredients(List<MissedIngredient> missedIngredients) {
        this.missedIngredients = missedIngredients;
    }

    public List<UsedIngredient> getUsedIngredients() {
        return usedIngredients;
    }

    public void setUsedIngredients(List<UsedIngredient> usedIngredients) {
        this.usedIngredients = usedIngredients;
    }

    public List<Object> getUnusedIngredients() {
        return unusedIngredients;
    }

    public void setUnusedIngredients(List<Object> unusedIngredients) {
        this.unusedIngredients = unusedIngredients;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

}
