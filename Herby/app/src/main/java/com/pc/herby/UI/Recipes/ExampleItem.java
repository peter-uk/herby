package com.pc.herby.UI.Recipes;

public class ExampleItem {
    private String mTitle;
    private int mRecipeID;

    public ExampleItem(String title, int recipeID){
        mTitle = title;
        mRecipeID = recipeID;
    }

    public String getTitle(){
        return mTitle;
    }

    public int getRecipeID(){
        return mRecipeID;
    }
}
