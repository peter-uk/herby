package com.pc.herby.data.local;


import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.pc.herby.tescoPOJO.QtyContents;

@Entity(tableName = "recipeFromIng")
public class RecipeFromIngDb {
    @PrimaryKey
    @NonNull
    private int id;

    private String title;

    private String image;

    private int usedIngredientCount;

    private int missedIngredientCount;

    @Embedded
    private QtyContents qtyContents;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUsedIngredientCount() {
        return usedIngredientCount;
    }

    public void setUsedIngredientCount(int usedIngredientCount) {
        this.usedIngredientCount = usedIngredientCount;
    }

    public int getMissedIngredientCount() {
        return missedIngredientCount;
    }

    public void setMissedIngredientCount(int missedIngredientCount) {
        this.missedIngredientCount = missedIngredientCount;
    }

    public QtyContents getQtyContents() {
        return qtyContents;
    }

    public void setQtyContents(QtyContents qtyContents) {
        this.qtyContents = qtyContents;
    }

    public RecipeFromIngDb(int id, String title, String image, int usedIngredientCount, int missedIngredientCount, QtyContents qtyContents) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.usedIngredientCount = usedIngredientCount;
        this.missedIngredientCount = missedIngredientCount;
        this.qtyContents = qtyContents;
    }

}
