package com.pc.herby.UI;

import android.content.Context;

import com.pc.herby.MainViewModelFactory;
import com.pc.herby.data.NetworkConnectionHelper;
import com.pc.herby.data.api.RecipeApiClient;
import com.pc.herby.data.api.RecipeApiService;
import com.pc.herby.data.api.TescoApiClient;
import com.pc.herby.data.api.TescoApiService;
import com.pc.herby.data.local.AppDatabase;
import com.pc.herby.repository.ProductsRepository;

public class Injection {

    public static TescoApiService getApiService(Context context){
        return TescoApiClient.getClient(context.getApplicationContext())
                .create(TescoApiService.class);
    }

    public static RecipeApiService getRApiService(Context context){
        return RecipeApiClient.getClient(context.getApplicationContext())
                .create(RecipeApiService.class);
    }

    public static AppDatabase getAppDatabase(Context context){
        return AppDatabase.getInstance(context.getApplicationContext());
    }

    public static NetworkConnectionHelper getConnectionHelper(Context context){
        return new NetworkConnectionHelper(context.getApplicationContext());
    }

    public static ProductsRepository getProductsRepository(Context context){
        return ProductsRepository.getInstance(
                getAppDatabase(context),
                getApiService(context),
                getConnectionHelper(context)
        );
    }


    public static MainViewModelFactory provideViewModelFactory(Context context, String gtin) {
        return new MainViewModelFactory(getProductsRepository(context), gtin);
    }
}
