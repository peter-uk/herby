package com.pc.herby.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.pc.herby.tescoPOJO.Product;
import com.pc.herby.data.NetworkConnectionHelper;
import com.pc.herby.data.api.TescoApiService;
import com.pc.herby.data.local.AppDatabase;
import com.pc.herby.data.local.ProductDb;

import java.util.List;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ProductsRepository {

    private static final String TAG = ProductsRepository.class.getSimpleName();
    private static volatile ProductsRepository INSTANCE;
    private AppDatabase appDatabase;
    private TescoApiService tescoApiService;
    private NetworkConnectionHelper connectionHelper;

    private ProductsRepository(AppDatabase appDatabase, TescoApiService tescoApiService, NetworkConnectionHelper connectionHelper) {
        this.appDatabase = appDatabase;
        this.tescoApiService = tescoApiService;
        this.connectionHelper = connectionHelper;
    }

    public static ProductsRepository getInstance(AppDatabase appDatabase,
                                                 TescoApiService tescoApiService,
                                                 NetworkConnectionHelper connectionHelper) {
        if (INSTANCE == null) {
            synchronized (ProductsRepository.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ProductsRepository(appDatabase, tescoApiService, connectionHelper);
                }
            }
        }
        return INSTANCE;
    }



    public LiveData<List<ProductDb>> getProductsListWithId(CompositeDisposable compositeDisposable,
                                                         String gtin) {

        if (connectionHelper.isNetworkConnected()) {
            compositeDisposable.add(Single.zip(tescoApiService.getSoup(gtin),
                    appDatabase.productDao().getRxProductsList(Long.valueOf(gtin))
                    , (results, localList) -> {
                        if (!results.getProducts().isEmpty()) {
                            List<Product> apiList = results.getProducts();
                            Product apiProduct = apiList.get(0);
                            if (localList == null || localList.isEmpty()) {
                                return ProductMapper.toProductDb(apiProduct);
                            } else {
                                ProductDb localProduct = localList.get(0);
                                double quantity = localProduct.getQtyContents().getQuantity();
                                double totalQuantity = localProduct.getQtyContents().getTotalQuantity();
                                //Heres adds current value to api return value
                                localProduct.getQtyContents().setQuantity(quantity + apiProduct.getQtyContents().getQuantity());
                                localProduct.getQtyContents().setTotalQuantity(totalQuantity + apiProduct.getQtyContents().getTotalQuantity());
                                return localProduct;
                            }
                        } else {
                            Log.i(TAG , "fetched list is empty : no available product with this gtin: "+ gtin);
                            return null;
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .flatMap(product -> {
                        if (product != null) {
                            return appDatabase.productDao().insert(product).subscribeOn(Schedulers.io());
                        } else {
                            return Single.just(0L);
                        }
                    })
                    .subscribe((aLong, throwable) -> {
                        if (throwable != null) {
                            Log.i(TAG, "error while fetching product with gtin : " + gtin + " Error: " + throwable.getMessage());
                        } else {
                            Log.i(TAG, "fetching product success with gtin : " + gtin);
                        }

                    }));
        }
        return appDatabase.productDao().getProductsList(Long.valueOf(gtin));
    }
}
