package com.pc.herby.UI.Recipes;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.pc.herby.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DetailActivity extends AppCompatActivity {
    private RequestQueue RequestQueue;
    private String instructions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");


        RequestQueue = Volley.newRequestQueue(this);
        parseJSON(id);

        CollapsingToolbarLayout recipe = findViewById(R.id.toolbar_layout);
        TextView scrolling = findViewById(R.id.ScrollingText);
        recipe.setTitle(intent.getStringExtra("title"));
        scrolling.setText(instructions);

    }

    private void parseJSON(String x) {
        String base_url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/";
        String url = base_url+x+"/information";;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject result = response.getJSONObject("instructions");
                            instructions = result.toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("x-rapidapi-host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
                headers.put("x-rapidapi-key", "9e74a9bde4msh6abebd4efa1c6cfp14e045jsna8a18a2ecf32");
                return headers;
            }
        };
        RequestQueue.add(request);
    }
}
