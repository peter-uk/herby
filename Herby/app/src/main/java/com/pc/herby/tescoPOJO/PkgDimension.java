
package com.pc.herby.tescoPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PkgDimension {

    @SerializedName("no")
    @Expose
    private Integer no;
    @SerializedName("height")
    @Expose
    private Double height;
    @SerializedName("width")
    @Expose
    private Double width;
    @SerializedName("depth")
    @Expose
    private Double depth;
    @SerializedName("dimensionUom")
    @Expose
    private String dimensionUom;
    @SerializedName("weight")
    @Expose
    private Double weight;
    @SerializedName("weightUom")
    @Expose
    private String weightUom;
    @SerializedName("volume")
    @Expose
    private Double volume;
    @SerializedName("volumeUom")
    @Expose
    private String volumeUom;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getDepth() {
        return depth;
    }

    public void setDepth(Double depth) {
        this.depth = depth;
    }

    public String getDimensionUom() {
        return dimensionUom;
    }

    public void setDimensionUom(String dimensionUom) {
        this.dimensionUom = dimensionUom;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getWeightUom() {
        return weightUom;
    }

    public void setWeightUom(String weightUom) {
        this.weightUom = weightUom;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getVolumeUom() {
        return volumeUom;
    }

    public void setVolumeUom(String volumeUom) {
        this.volumeUom = volumeUom;
    }

}
