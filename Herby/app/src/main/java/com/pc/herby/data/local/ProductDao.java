package com.pc.herby.data.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM product WHERE gtin = :gtin ")
    LiveData<List<ProductDb>> getProductsList(Long gtin);

    @Query("SELECT * FROM product WHERE gtin = :gtin ")
    LiveData<ProductDb> getProduct(Long gtin);

    @Query("SELECT * FROM product WHERE gtin = :gtin ")
    Single<List<ProductDb>> getRxProductsList(Long gtin);

    @Insert(onConflict = REPLACE)
    Single<Long> insert(ProductDb item);

    @Update
    Single<Integer> update(ProductDb item);

    @Insert(onConflict = REPLACE)
    Single<List<Long>> insertAll(List<ProductDb> list);
}
