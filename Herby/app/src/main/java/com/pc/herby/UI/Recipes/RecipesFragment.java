package com.pc.herby.UI.Recipes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.pc.herby.R;
import com.pc.herby.UI.Home.HomeAdapter;
import com.pc.herby.UI.Home.HomeViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RecipesFragment extends Fragment implements ExampleAdapter.onItemClickListener {

    private RecyclerView mRecylerView;
    private ExampleAdapter myAdapter;
    private ArrayList<ExampleItem> mExampleList;
    private RecipesViewModel RecipesViewModel;
    private RequestQueue mRequestQueue;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recipes, container, false);

        mExampleList = new ArrayList<>();

        mRecylerView = root.findViewById(R.id.RecipeRecyclerView);
        mRecylerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRequestQueue = Volley.newRequestQueue(Objects.requireNonNull(getActivity()).getApplicationContext());
        parseJSON();

        return root;
 }

 private void parseJSON() {
     String url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/complexSearch?number=25&ranking=1&includeIngredients=chicken%252C%20bread";
     JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
             new Response.Listener<JSONObject>() {
                 @Override
                 public void onResponse(JSONObject response) {
                     try {
                         JSONArray jsonArray = response.getJSONArray("results");
                         for (int i = 0; i < jsonArray.length(); i++) {
                             JSONObject result = jsonArray.getJSONObject(i);
                             String recipeName = result.getString("title");
                             int id = result.getInt("id");
                             mExampleList.add(new ExampleItem(recipeName, id));
                         }
                         myAdapter = new ExampleAdapter(Objects.requireNonNull(getActivity()).getApplicationContext(),mExampleList);
                         mRecylerView.setAdapter(myAdapter);
                         myAdapter.setOnItemClickListener(RecipesFragment.this);
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
             }, new Response.ErrorListener() {
         @Override
         public void onErrorResponse(VolleyError error) {
         }
     }) {
         @Override
         public Map getHeaders() throws AuthFailureError {
             HashMap headers = new HashMap();
             headers.put("x-rapidapi-host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
             headers.put("x-rapidapi-key", "9e74a9bde4msh6abebd4efa1c6cfp14e045jsna8a18a2ecf32");
             return headers;
         }
     };
     mRequestQueue.add(request);
 }

    @Override
    public void onItemClick(int position) {
        Intent Detail = new Intent(getActivity(), DetailActivity.class);
        ExampleItem clickedItem = mExampleList.get(position);
        Detail.putExtra("id",String.valueOf(clickedItem.getRecipeID()));
        Detail.putExtra("title",clickedItem.getTitle());
        startActivity(Detail);
    }
}
