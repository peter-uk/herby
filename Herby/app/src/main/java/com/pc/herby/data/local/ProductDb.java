package com.pc.herby.data.local;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.pc.herby.tescoPOJO.QtyContents;

@Entity(tableName = "product")
public class ProductDb {
    @PrimaryKey
    @NonNull
    private Long gtin;

    private String tpnb;

    private String tpnc;

    private String description;

    private String brand;

    @Embedded
    private QtyContents qtyContents;

    @NonNull
    public Long getGtin() {
        return gtin;
    }

    public void setGtin(@NonNull Long gtin) {
        this.gtin = gtin;
    }

    public String getTpnb() {
        return tpnb;
    }

    public void setTpnb(String tpnb) {
        this.tpnb = tpnb;
    }

    public String getTpnc() {
        return tpnc;
    }

    public void setTpnc(String tpnc) {
        this.tpnc = tpnc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public QtyContents getQtyContents() {
        return qtyContents;
    }

    public void setQtyContents(QtyContents qtyContents) {
        this.qtyContents = qtyContents;
    }

    public ProductDb(@NonNull Long gtin, String tpnb, String tpnc, String description, String brand, QtyContents qtyContents) {
        this.gtin = gtin;
        this.tpnb = tpnb;
        this.tpnc = tpnc;
        this.description = description;
        this.brand = brand;
        this.qtyContents = qtyContents;
    }
}
