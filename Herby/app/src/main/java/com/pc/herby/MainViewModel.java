package com.pc.herby;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.pc.herby.data.local.ProductDb;
import com.pc.herby.repository.ProductsRepository;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class MainViewModel extends ViewModel {

    private ProductsRepository productsRepository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private LiveData<List<ProductDb>> productList;

/*
    every time this view model is created, the productsList will be fetched if network connection is available
    and the value of this product will be updated
*/

    public MainViewModel(ProductsRepository productsRepository,
                         String gtin) {
        this. productsRepository = productsRepository;
        productList = productsRepository.getProductsListWithId(compositeDisposable, gtin);
    }

    public LiveData<List<ProductDb>> getProductList() {
        return productList;
    }


    //it's so important to override this method and dispose the compositeDisposable
    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
