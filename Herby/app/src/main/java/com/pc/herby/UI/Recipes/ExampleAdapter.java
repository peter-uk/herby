package com.pc.herby.UI.Recipes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pc.herby.R;

import java.util.ArrayList;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {
    private Context mContext;
    private ArrayList<ExampleItem> mExampleItems;
    private onItemClickListener mListener;

    public interface onItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(onItemClickListener listener){
        mListener = listener;
    }

    public ExampleAdapter(Context context, ArrayList<ExampleItem> exampleItems){
        mContext = context;
        mExampleItems = exampleItems;
    }

    public class ExampleViewHolder extends RecyclerView.ViewHolder{
        public TextView mRecipeName;
        public TextView mRecipeID;

        public ExampleViewHolder(@NonNull View itemView) {
            super(itemView);
            mRecipeName = itemView.findViewById(R.id.recipe_title);
            mRecipeID = itemView.findViewById(R.id.recipe_ID);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener != null){
                      int position = getAdapterPosition();
                      if(position != RecyclerView.NO_POSITION){
                          mListener.onItemClick(position);
                      }
                    }
                }
            });
            }
        }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.recipe_item,parent,false);
        return new ExampleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        holder.mRecipeName.setText(mExampleItems.get(position).getTitle());
        holder.mRecipeID.setText(String.valueOf(mExampleItems.get(position).getRecipeID()));
    }

    @Override
    public int getItemCount() {
        return mExampleItems.size();
    }


}
