package com.pc.herby;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.pc.herby.MainViewModel;
import com.pc.herby.repository.ProductsRepository;

public class MainViewModelFactory implements ViewModelProvider.Factory{

    private final ProductsRepository repository;
    private String gtin;

    public MainViewModelFactory(ProductsRepository repository, String gtin) {
        this.repository = repository;
        this.gtin = gtin;
    }

    @Override
    @NonNull
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(repository, gtin);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
