package com.pc.herby.data.api;


import com.pc.herby.tescoPOJO.Results;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TescoApiService {



    @GET("product/")
    Single<Results> getSoup(@Query("gtin") String gtin);

}
