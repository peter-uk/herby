**Design Ideas**

------

| Apps         |                           |
| ------------ | ------------------------- |
| Instagram    | * Scrollable recipes      |
| Myfitnesspal | * Nutritional information |



------

**Camera Solutions**

------

**Google Vision API**

| Feature                 | Description                                                  |
| ----------------------- | ------------------------------------------------------------ |
| LABEL_DETECTION         | Add labels based on image content                            |
| TEXT_DETECTION          | Perform Optical Character Recognition (OCR) on text within the image |
| DOCUMENT_TEXT_DETECTION | Perform OCR on dense text images, such as documents          |
| OBJECT_LOCALIZATION     | Detect and extract multiple objects in an image              |
| LOGO_DETECTION          | Detect company logos within the image                        |
| FACE_DETECTION          | Detect faces within the image                                |

------

**Google ML Kit**

| Firebase       |                                                              |
| -------------- | ------------------------------------------------------------ |
| Barcodes       | https://firebase.google.com/docs/ml-kit/android/read-barcodes |
| Image labeling | https://developers.google.com/ml-kit/vision/image-labeling   |

------

**APIs**

| API                       | Recipes Amount | Cost                                       | Response time | Extras                   |
| ------------------------- | -------------- | ------------------------------------------ | ------------- | ------------------------ |
| Recipe Puppy              | >1,000,000     | Free                                       | 220ms         |                          |
| Recipe - Food - Nutrition | >365,000       | Free: 500/Results 50/Requests 500/Tinyreqs | 412ms         | natural language queries |
| Chomp                     | >535,000       | $19.00/MO                                  | ~             |                          |
| BigOven                   | >350,000       | Free                                       | 347ms         |                          |

------

Storing Data

| Room                                                         |
| ------------------------------------------------------------ |
| https://developer.android.com/training/data-storage/room     |
| Room provides an abstraction layer over SQLite to allow fluent database access while harnessing the full power of SQLite |
|                                                              |

------

Development Goals

1. UI 
2. Scanning
3. Storing scanned items
4. Searching API
5. Storing API response
6. Displaying recipes on screen 