# Herby

This project is an Android Application to suggest recipes to a user based on the ingredients they already have in their home. The unique selling point for this Application is it taps into recipes the user didn’t know they had from ingredients they already have, and also a ingredient inventory manager which users of the system can manage at home and while shopping.

## Getting Started

These instructions will get you a copy of the project up and running on your android device for testing purposes.

### Prerequisites


```
An Android Device 
Herby APK
```

### Installing

Enable External sources 
```
Go to your phone’s Settings
Go to Security & privacy > More settings
Tap on Install apps from external sources
Select the browser you want to download APK files from
Toggle Allow app installs ON
```

Installing APK
```
Download APK 
Install APK from download folder
Run Herby once installed
```
[Download APK Here](https://cseegit.essex.ac.uk/ce301_2019/ce301_cross_p/-/blob/master/Herby/app/build/outputs/apk/debug/app-debug.apk)

## Built With

* [CameraKit](https://github.com/CameraKit/camerakit-android) - Camera framework
* [ML Kit](https://developers.google.com/ml-kit) - Handling of barcodes and opject detection
* [Google Vision](https://cloud.google.com/vision/) - Used to read text and handwriting
* [Room](https://developer.android.com/topic/libraries/architecture/room) - Database handler


## Authors

* **Peter Cross** 



## Acknowledgments

* [Azura Group](https://www.azuragroup.com/) - Project Idea
